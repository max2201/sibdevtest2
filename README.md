# sibdev-test2

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
user logins & passwords from users.json:
"username": "test",
"password": "test"

"username": "max",
"password": "max"

"username": "kek",
"password": "kee"
