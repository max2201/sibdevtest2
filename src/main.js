import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.use(ElementUI)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

store.subscribe((mutation, state) => {
  if (localStorage.getItem('flag')) {
    localStorage.setItem(JSON.parse(localStorage.getItem('user')).id, JSON.stringify(state))
  }
})
