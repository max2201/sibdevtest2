export default {
  state: {
    loading: true
  },
  mutations: {
    changeLoadingState (state, loading) {
      state.loading = loading
    }
  },
  actions: {}
}
