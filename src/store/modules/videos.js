import { getVideos } from '@/api/youTubeVideos.js'

export default {
  namespaced: true,
  state: {
    requestText: '',
    requestedVideos: [
    ],
    prevPageToken: '',
    nextPageToken: '',
    totalResults: 0
  },
  mutations: {
    updateVideosInfo (state, responseData) {
      console.log(responseData)
      state.requestText = responseData.requestText
      if (responseData.requestText) {
        state.requestedVideos = responseData.data.items
        state.prevPageToken = responseData.data.prevPageToken
        state.nextPageToken = responseData.data.nextPageToken
        state.totalResults = responseData.data.pageInfo.totalResults
      }
    },
    resetVideosInfo (state) {
      state.requestText = ''
      state.requestedVideos = []
      state.prevPageToken = ''
      state.nextPageToken = ''
      state.totalResults = 0
    }

  },
  actions: {

    async search ({ commit }, request) {
      const responseData = {
        requestText: request.requestText,
        ...(request.requestText ? await getVideos(request) : {})
      }
      commit('updateVideosInfo', responseData)
    }
  }
}
