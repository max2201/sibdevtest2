import { userActions } from '@/api/authorization.js'

const user = JSON.parse(localStorage.getItem('user'))
const initialState = user ? { status: { loggedIn: true }, user }
  : { status: { }, user: null }

export default {
  namespaced: true,
  state: initialState,
  mutations: {
    loginSuccess (state, user) {
      state.status = { loggedIn: true }
      state.user = user
    },
    loginFailure (state) {
      state.status = {}
      state.user = null
    },
    logout (state) {
      state.status = {}
      state.user = null
    }
  },
  actions: {
    async login ({ commit }, userCredentials) {
      try {
        const user = await userActions.login(userCredentials)
        if (user.token) {
          commit('loginSuccess', user.username)
          commit('initialiseStore', user.id, { root: true })
        } else {
          alert('Ошибка: нет токена')
        }
      } catch (error) {
        error.response.status === 401 ? alert('Не правильный логин или пароль') : alert('Ошибка')
        commit('loginFailure')
      }
    },
    async logout ({ commit }) {
      await userActions.logout()
      commit('logout')
      commit('videosModule/resetVideosInfo', null, { root: true })
      commit('requestsModule/resetRequests', null, { root: true })
    }
  }
}
