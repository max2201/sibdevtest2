export default {
  namespaced: true,
  state: {
    requests: []
  },
  mutations: {
    deleteRequest (state, requestIndex) {
      state.requests.splice(requestIndex, 1)
    },
    saveRequest (state, requestData) {
      state.requests.push(requestData)
    },
    updateRequest (state, updateData) {
      state.requests[updateData.targetRequestId] = updateData.newRequestData
    },
    resetRequests (state) {
      state.requests = []
    }
  },
  // actions: {
  //   saveRequest ({ commit }, requestData) {
  //     commit('saveRequest', requestData)
  //   },
  //   deleteRequest ({ commit }, id) {
  //     commit('deleteRequest', id)
  //   },
  //   updateRequest ({ commit }, updateData) {
  //     commit('updateRequest', updateData)
  //   }
  // },
  getters: {
    requestIndex: state => (targetRequest, searchBy) => {
      if (searchBy === 'name') {
        return state.requests.indexOf(state.requests.find(request => request.name === targetRequest.name))
      } else if (searchBy === 'requestText') {
        return state.requests.indexOf(state.requests.find(request => request.requestText.toLowerCase() === targetRequest.toLowerCase()))
      }
    },
    isRequestInList: state => (requestText, searchBy) => {
      if (searchBy === 'name') {
        return state.requests.some(request => request.name === requestText)
      } else if (searchBy === 'requestText') {
        return state.requests.some(request => request.requestText.toLowerCase() === requestText.toLowerCase())
      }
    }
  }
}
