import Vue from 'vue'
import Vuex from 'vuex'
import generalModule from './modules/general.js'
import authorizationModule from './modules/authorization.js'
import requestsModule from './modules/saved-requests.js'
import videosModule from './modules/videos'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {
    initialiseStore (state, index) {
      if (localStorage.getItem(index)) {
        this.replaceState(
          Object.assign(state, JSON.parse(localStorage.getItem(index)))// з амена текущего состояния состоянием юзера
        )
        localStorage.setItem('flag', '1')
      } else {
        localStorage.setItem('flag', '1')
      }
    }
  },
  actions: {},
  modules: {
    generalModule,
    authorizationModule,
    requestsModule,
    videosModule
  }
})
