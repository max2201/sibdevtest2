import Vue from 'vue'
import VueRouter from 'vue-router'
import Authorization from '@/views/Authorization'
import Search from '@/views/Search'
import Favourites from '@/views/Favourites'
Vue.use(VueRouter)
// comment
const routes = [
  {
    path: '*',
    redirect: '/authorization'
  },
  {
    path: '/authorization',
    name: 'Authorization',
    component: Authorization
  },
  {
    path: '/search',
    name: 'Search',
    component: Search
  },
  {
    path: '/favourites',
    name: 'Favourites',
    component: Favourites
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  // redirect to login page if not logged in and trying to access a restricted page
  const publicPages = ['/authorization']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = localStorage.getItem('user')
  if (authRequired && !loggedIn) {
    return next('/authorization')
  }
  next()
})

export default router
