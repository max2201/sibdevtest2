import API from './axios-ytube-settings.js'
import { YOU_TUBE_API_KEY, YOU_TUBE_API_ADDRESS } from '@/constants'

export function getVideos (requestParams) {
  const api = {
    baseUrl: YOU_TUBE_API_ADDRESS,
    part: 'snippet',
    type: 'video',
    order: requestParams.sortBy,
    maxResults: requestParams.maximumAmount,
    q: requestParams.requestText,
    key: YOU_TUBE_API_KEY
  }
  const apiUrl = `${api.baseUrl}part=${api.part}&type=${api.type}&order=${api.order}&q=${api.q}&maxResults=${api.maxResults}&key=${api.key}`
  return API.get(apiUrl)
}
