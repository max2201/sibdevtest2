import axios from 'axios'
// import authHeader from '../api/auth-header.js'
import { YOU_TUBE_API_ADDRESS } from '@/constants'

const API = axios.create({
  baseURL: YOU_TUBE_API_ADDRESS
})
export default API
// const serverRequest = axios.create({
//   baseURL: API_ADDRESS,
//   headers: {
//     Authorization: `${authHeader()}`,
//     'Content-Type': 'application/json'
//   }
// })
