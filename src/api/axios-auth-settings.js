import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import usersFile from '../users.json'

const AUTH = axios.create({
  baseURL: '',
  headers: { 'Content-Type': 'application/json' }
})

const mock = new MockAdapter(AUTH, { delayResponse: 1000 })

mock.onPost('/authenticate').reply(function (axiosDataConfig) {
  const data = JSON.parse(axiosDataConfig.data)
  const users = usersFile
  const filteredUsers = users.filter(user => {
    return user.username === data.username && user.password === data.password
  })
  if (filteredUsers.length) {
    const user = filteredUsers[0]
    const responseJson = {
      id: user.id,
      username: user.username,
      token: 'fake-jwt-token'
    }
    return [200, JSON.stringify(responseJson)]
  } else { return [401, {}] }
}
)

export default AUTH
