import auth from '../api/axios-auth-settings.js'
export const userActions = {
  login,
  logout
}
function login (userCredentials) {
  return auth.post('/authenticate', userCredentials).then(response => {
    if (response.data.token) {
      localStorage.setItem('user', JSON.stringify(response.data))
    } else logout()
    return response.data
  })
}
function logout () {
  localStorage.removeItem('user')
  localStorage.removeItem('flag')
}
