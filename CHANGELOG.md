# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.12](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.11...v0.1.12) (2023-11-02)


### Features

* add comment ([02f8ac8](https://gitlab.com/max2201/sibdevtest2/commit/02f8ac8dd5861e0c0d549f455b6272aef2f89fe8))
* add comment ([abb3c14](https://gitlab.com/max2201/sibdevtest2/commit/abb3c146c95ffaa7468a5eb4c0dd8f8e0b7bf2e0))

### [0.1.11](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.10...v0.1.11) (2023-11-02)


### Features

* add comment ([ea4dd47](https://gitlab.com/max2201/sibdevtest2/commit/ea4dd4768ea02f657302a2a48f90e56ed57cbca2))

### [0.1.10](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.9...v0.1.10) (2023-11-02)


### Features

* add comment ([c5aea5d](https://gitlab.com/max2201/sibdevtest2/commit/c5aea5db45d1dc504dbe9138007f5429c0d25945))

### [0.1.9](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.8...v0.1.9) (2023-11-02)


### Features

* add comment ([d28e05f](https://gitlab.com/max2201/sibdevtest2/commit/d28e05f52cc25aab8d80310822715e03136bc32f))

### [0.1.8](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.7...v0.1.8) (2023-11-02)


### Features

* add comment ([59171e5](https://gitlab.com/max2201/sibdevtest2/commit/59171e52dfde0080cffb54baf2ea947d37dffc4a))

### [0.1.7](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.6...v0.1.7) (2023-11-02)

### [0.1.6](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.5...v0.1.6) (2023-11-02)


### Features

* add comment ([2584260](https://gitlab.com/max2201/sibdevtest2/commit/2584260170b5100005a459df73ca4743fb6a6f30))

### [0.1.5](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.4...v0.1.5) (2023-11-02)


### Features

* add comment ([3524ce5](https://gitlab.com/max2201/sibdevtest2/commit/3524ce5cb5f997ad4dd3f9bf793e26b12d099429))

### [0.1.4](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.3...v0.1.4) (2023-11-02)


### Bug Fixes

* del comment ([3a98842](https://gitlab.com/max2201/sibdevtest2/commit/3a98842b00aaaaf22937442bab5671580feb9f20))

### [0.1.3](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.2...v0.1.3) (2023-11-02)


### Features

* add comment ([7f3ccc5](https://gitlab.com/max2201/sibdevtest2/commit/7f3ccc53dbed9861d4b5de12b2c8a5b58d05c3af))
* add comment ([6052cbc](https://gitlab.com/max2201/sibdevtest2/commit/6052cbca1944ae6971a4946d86a4e3cb0d1617ca))
* add comment ([6f4d8e6](https://gitlab.com/max2201/sibdevtest2/commit/6f4d8e68049b64b613710e6d4eadc0f65b58b41a))


### Bug Fixes

* del comment ([22ff992](https://gitlab.com/max2201/sibdevtest2/commit/22ff992053921711c6722b73fe6e83cb463f916b))
* del comment ([6449f34](https://gitlab.com/max2201/sibdevtest2/commit/6449f34db62696f831b72c70ecf134587a036174))
* del comment ([d10597a](https://gitlab.com/max2201/sibdevtest2/commit/d10597ad8ef1f02319ba95930b0fdeac135db851))

### [0.1.2](https://gitlab.com/max2201/sibdevtest2/compare/v0.1.1...v0.1.2) (2023-11-02)

### 0.1.1 (2023-11-02)


### Features

* add comment ([99e4031](https://gitlab.com/max2201/sibdevtest2/commit/99e403150cb111627d6343b82a17c0b482abb7dd))
